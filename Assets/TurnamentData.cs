﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnamentData : MonoBehaviour
{
    public static TurnamentData instance;
    public bool IsTurnamentLevel;

    private void Awake()
    {
        if (instance == null)
        { // Экземпляр менеджера был найден
            instance = this; // Задаем ссылку на экземпляр объекта
        }
        else if (instance == this)
        { // Экземпляр объекта уже существует на сцене
            Destroy(gameObject); // Удаляем объект
        }

        // Теперь нам нужно указать, чтобы объект не уничтожался
        // при переходе на другую сцену игры
        DontDestroyOnLoad(gameObject);
    }
}
