﻿using UnityEngine;

public class ShakerManager : MonoBehaviour
{
    public static ShakerManager instance;

    public CinemachineCameraShaker[] shakers;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    public void Shake()
    {
        foreach (CinemachineCameraShaker shaker in shakers)
        {
            shaker.ShakeCamera(0.2f);
        }
    }
}
