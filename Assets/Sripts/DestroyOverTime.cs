﻿using UnityEngine;

public class DestroyOverTime : MonoBehaviour
{
    public float lifeTime;

    private void Awake()
    {
        Destroy(gameObject, lifeTime);
    }
}
