﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [HideInInspector] public bool reward;
    [HideInInspector] public bool gift;

    public LevelInfo levelInfo;
    public GameObject[] ground;

    [System.Serializable]
    public enum State
    {
        Waiting, Player, Enemy
    }
    public State gameState;

    [System.Serializable]
    public enum GameType
    {
        Normal,
        Turnament
    }
    public State gameType;


    public int leftBound, rightBound;
    public Sprite[] catEnemies;

    [Header("Level Setup")]
    public Transform playerSpawnPoint;
    public List<Transform> enemySpawnsPoint;
    public UIHealthbar playerHB;
    public GameObject EnemyHBHolder;
    public GameObject enemyHBPrefub;
    public List<UIHealthbar> enemysHB;
    public GameObject dots; 
    private int EnemyCount=3;    
    [HideInInspector]
    public bool gameOver = false, playerWon = false;
    [HideInInspector]
    public List<AIEnemy> enemys;
    [HideInInspector]
    public Player player;

    [HideInInspector]
    public State nextState;

    public Cinemachine.CinemachineTargetGroup targetGroup;

    [Space]
    public WeaponManager weaponManager;
    public TanksManager tanksManager;
    public UIManager uIManager;

    private void Awake()
    {
        enemys = new List<AIEnemy>();
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
        if (ground.Length != 0)
            ground[UnityEngine.Random.Range(0, ground.Length - 1)].SetActive(true);
    }

    private void Start()
    {
        
        SpawnPlayers();

        Invoke("ToPlayer", 5);
    }

    private void SpawnPlayers()
    {
        // Spawn Player
        TankInfo playerTankInfo = tanksManager.GetTank(PlayerPrefs.GetString("Tank In Use", "T-34"));

        GameObject playerObj = Instantiate(playerTankInfo.prefab, playerSpawnPoint.position, Quaternion.identity);
        playerObj.name = "Player";
        player = playerObj.AddComponent<Player>();
        player.aimDrawer = dots;
        player.tag = "Player";
        playerHB.target = player.GetComponent<Health>();
        playerHB.target.health = TurnamentData.instance.IsTurnamentLevel?playerHB.target.health*3:playerHB.target.health;
        playerHB.gameObject.SetActive(true);
        dots.GetComponent<TankAim_2>().target = playerObj.GetComponent<TankAim>();
        dots.GetComponent<TankAim_2>().ballPos = playerObj.GetComponent<TankParts>().firePoint;

        Cinemachine.CinemachineTargetGroup.Target playerTarget = new Cinemachine.CinemachineTargetGroup.Target();
        playerTarget.target = player.transform;
        playerTarget.radius = 1;
        playerTarget.weight = 1;
        targetGroup.m_Targets.SetValue(playerTarget, 0);

        weaponManager.player = playerObj.GetComponent<TankWeapons>();
        weaponManager.SetPlayerWeapon(null); 
        if (TurnamentData.instance.IsTurnamentLevel)
        {
            for (int i = 0; i < EnemyCount; i++)
            {
                SpawnEnemy(i, enemySpawnsPoint[i], enemysHB[i], uIManager);
            }
        }
        else
        {
            SpawnEnemy(0, enemySpawnsPoint[0], enemysHB[0], uIManager);
        }


        // Camera stuff
        foreach (var enemy in enemys)
        {
            Cinemachine.CinemachineTargetGroup.Target enemyTarget = new Cinemachine.CinemachineTargetGroup.Target();
            enemyTarget.target = enemy.transform;
            enemyTarget.radius = 1;
            enemyTarget.weight = 1;
            targetGroup.m_Targets.SetValue(enemyTarget, 1);
        }
    }

    private void SpawnEnemy(int id, Transform enemySpawnPoint, UIHealthbar enemyHB, UIManager UiManager)
    {
        TankInfo enemyTankInfo = tanksManager.GetRandomTank();

        GameObject enemyObj = Instantiate(enemyTankInfo.prefab, enemySpawnPoint.position, Quaternion.identity);
        enemyObj.name = "Enemy";
        enemys.Add(enemyObj.AddComponent<AIEnemy>());
        enemys[id].Id = id;
        enemys[id].tag = "Enemy";
        enemyHB.target = enemys[id].GetComponent<Health>();
        enemyHB.gameObject.SetActive(true);
        int r = UnityEngine.Random.Range(0, catEnemies.Length);
        enemyObj.GetComponent<TankParts>().catPlace.sprite = catEnemies[r];
        UiManager.enemyIcon.sprite = catEnemies[r];
        enemys[id].Init();

    }

    public void ToPlayer()
    {
        gameState = State.Player;

        UIManager.instance.playerUI.SetActive(true);
        player.Activate();
    }

    private int counter = 0;
    public void ToEnemy()
    {
        gameState = State.Enemy;

        foreach (var enemy in enemys)
        {
            if (enemy != null)
                enemy.Activate(counter);
        }


        if (counter + 1 >= enemys.Count)
        {
            counter = 0;
        }
        else
        {
            counter++;
        }
    }

    public void Wait()
    {
        if (gameState == State.Player)
            nextState = State.Enemy;
        else if (gameState == State.Enemy)
            nextState = State.Player;

        gameState = State.Waiting;

        UIManager.instance.playerUI.SetActive(false);
    }

    public void NextState(float wait)
    {
        Invoke("InternalNextState", wait);
    }

    private void InternalNextState()
    {
        if (nextState == State.Player)
            ToPlayer();
        else if (nextState == State.Enemy)
            ToEnemy();
    }

    public void PlayerShoot()
    {
        player.Shoot();
    }

    public void PlayerSetOffset()
    {
        player.tankAim.SetOffset();
    }

    public void PlayerClearOffset()
    {
        player.tankAim.ClearOffset();
    }

    public void PLayerWon()
    {
        Advertisements.Instance.ShowInterstitial();
        if (enemys.Count > 0) return;

        if (gameOver)
            return;

        playerWon = true;
        gameOver = true;

        PlayerPrefs.SetInt("Coins Earned", UnityEngine.Random.Range(100, 400));

        PlayerPrefs.SetInt(levelInfo.levelName + " progress", PlayerPrefs.GetInt(levelInfo.levelName + " progress", 0) + 1);

        StartCoroutine(PlayrerWonSteps());
    }

    private IEnumerator PlayrerWonSteps()
    {
        yield return new WaitForSeconds(3);

        LevelLoader.instance.LoadLevel(1);
    }

    public void PlayerLost()
    {
        Advertisements.Instance.ShowInterstitial();

        if (gameOver)
            return;

        playerWon = false;
        gameOver = true;

        StartCoroutine(PlayerLostSteps());
    }

    private IEnumerator PlayerLostSteps()
    {
        yield return new WaitForSeconds(3);

        UIManager.instance.lostPanel.SetActive(true);
    }
}
