﻿using UnityEngine;

public class MaterialScroller : MonoBehaviour
{
    public Material material;
    public float scrollSpeed = 10;

    private float offset = 0;

    private void Start()
    {
        material.mainTextureOffset = Vector2.zero;
    }

    private void Update()
    {
        offset += scrollSpeed * Time.deltaTime;

        material.mainTextureOffset = Vector2.right * offset;
    }
}
