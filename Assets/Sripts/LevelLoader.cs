﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour
{
    public static LevelLoader instance;

    public Animator anim;
    public int selectedLevelID; 

    private void Awake()
    {
        if(instance==null)
                instance = this; 
         
    }

    public void LoadLevel(int index)
    {
        StartCoroutine(LoadWithTransition(index));
    }

    private IEnumerator LoadWithTransition(int index)
    {
        anim.SetTrigger("Load");

        yield return new WaitForSeconds(0.5f);

        SceneManager.LoadScene(index);
    }

    public void PlaySelectedLevel()
    {
        TurnamentData.instance.IsTurnamentLevel = false;
        StartCoroutine(LoadWithTransition(selectedLevelID));
    }
    public void PlaySelectedLevelAsTurnament()
    {
        TurnamentData.instance.IsTurnamentLevel = true;
        StartCoroutine(LoadWithTransition(selectedLevelID));
    }


    public void RestartLevel()
    {
        StartCoroutine(LoadWithTransition(SceneManager.GetActiveScene().buildIndex));
    }
     
}
