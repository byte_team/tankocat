﻿using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    public AudioSource audioSource;
    public AudioMixer mainMixer;

    [Header("UI Sounds")]
    public AudioClip button;
    public AudioClip start, buy, confirm, denied;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();
    }

    public void ButtonSound()
    {
        audioSource.PlayOneShot(button);
    }

    public void StartSound()
    {
        audioSource.PlayOneShot(start);
    }

    public void BuySound()
    {
        audioSource.PlayOneShot(buy);
    }

    public void ConfirmSound()
    {
        audioSource.PlayOneShot(confirm);
    }

    public void DeniedSound()
    {
        audioSource.PlayOneShot(denied);
    }
}
