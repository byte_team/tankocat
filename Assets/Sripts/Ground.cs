﻿using UnityEngine.U2D;
using UnityEngine;
using System.Collections.Generic;

public class Ground : MonoBehaviour
{
    public static Ground instance;

    [Header("Terrain Settings")]
    public float pointInterval = 1;

    public Collider2D terrainCollider;
    private Spline spline;
    private List<Vector3> holePoints = new List<Vector3>();
    private SpriteShapeController shapeController;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        shapeController = GetComponent<SpriteShapeController>();
        spline = shapeController.spline;
    }

    public void Deform (Vector3 center, float radius, Collider2D expCollider)
    {
        for (int i = 0; i < spline.GetPointCount(); i++)
        {
            Vector2 point = spline.GetPosition(i);

            if (expCollider.OverlapPoint(point))
            {
                /**Vector2 dir = point - (Vector2)center;
                dir.Normalize();
                if (dir.y > 0)
                    dir.y = -dir.y;

                Vector2 newPos = (Vector2)center + (dir * radius);

                spline.SetPosition(i, newPos);*/

                Vector2 newPos = point;
                newPos.y -= radius;

                spline.SetPosition(i, newPos);
            }
        }
    }
}
