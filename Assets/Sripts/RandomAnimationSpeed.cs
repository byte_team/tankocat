﻿using UnityEngine;

public class RandomAnimationSpeed : MonoBehaviour
{
    public float minValue = 0.5f, maxValue = 2f;

    private void Awake()
    {
        GetComponent<Animator>().SetFloat("Speed", Random.Range(minValue, maxValue));    
    }
}
