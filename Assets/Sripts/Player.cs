﻿using System;
using UnityEngine;

[RequireComponent(typeof(TankController))]
[RequireComponent(typeof(TankAim))]
[RequireComponent(typeof(TankWeapons))]
[RequireComponent(typeof(Health))]

public class Player : MonoBehaviour
{
    public GameObject aimDrawer;

    private TankController tankController;
    [HideInInspector]
    public TankAim tankAim;
    [HideInInspector]
    public TankWeapons tankWeapons;
    private Health health;
    private FixedJoystick moveJoystick;

    private void OnEnable()
    {
        health.OnDeath += Death;
    }

    private void OnDisable()
    {
        health.OnDeath -= Death;
    }

    private void Awake()
    {
        tankController = GetComponent<TankController>();
        tankAim = GetComponent<TankAim>();
        tankWeapons = GetComponent<TankWeapons>();
        health = GetComponent<Health>();
        moveJoystick = FindObjectOfType<FixedJoystick>();
    }

    private void Update()
    {
        if (GameManager.instance.gameState != GameManager.State.Player)
        {
            if (aimDrawer.activeSelf)
            {
                tankController.SpeedInput(0);
                aimDrawer.SetActive(false);
            }
            return;
        }

        tankController.SpeedInput(moveJoystick.Horizontal);
        UIManager.instance.UpdateFuel(tankController.gas, tankController.maxGas);

        if (moveJoystick.Horizontal == 0 && !aimDrawer.activeSelf)
        {
            aimDrawer.SetActive(true);
        }
        else if (moveJoystick.Horizontal != 0 && aimDrawer.activeSelf)
        {
            aimDrawer.SetActive(false);
        }
    }

    private void Death()
    {
        GameManager.instance.PlayerLost();

        Destroy(gameObject, 0.1f);
    }

    public void Shoot()
    {
        if (GameManager.instance.gameState == GameManager.State.Player)
        {
            tankAim.power = tankAim.power >= 0.1f ? tankAim.power : 0.1f;
            tankWeapons.Shoot(tankAim.aim.right * tankAim.power * tankWeapons.weapon.speed * tankWeapons.weapon.speed); 
            GameManager.instance.Wait();
        }
    }

    public void Activate ()
    {
        if (tankController == null)
            return;

        health.EfectCounter();
        tankController.ResetFuel();
        tankController.canMove = true;

        if (moveJoystick == null)
            moveJoystick = FindObjectOfType<FixedJoystick>();
    }
}
