﻿using UnityEngine;

public class TanksManager : MonoBehaviour
{
    public static TanksManager instance;

    public TankInfo[] tanks;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    public TankInfo GetTank(string tankName)
    {
        for(int i = 0; i < tanks.Length; i++)
        {
            if (tanks[i].tankName == tankName)
                return tanks[i];
        }

        print("Couldn't find " + tankName);
        return tanks[0];
    }

    public TankInfo GetRandomTank()
    {
        int r = Random.Range(0, tanks.Length);

        return tanks[r];
    }
}
