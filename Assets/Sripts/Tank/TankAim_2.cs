﻿using UnityEngine;

public class TankAim_2 : MonoBehaviour
{
    public TankAim target;

    public float drawBound = 10;
    public float dotSeparation;
    public float dotShift;
    public Transform[] dots;
    public Transform ballPos;

    private int numberOfDots;
    private float x1, y1;
    private bool draw = false;

    private void Awake()
    {
        numberOfDots = dots.Length;
    }

    private void Update()
    {
        if (target == null)
        {
            gameObject.SetActive(false);
            return;
        }

        Vector2 shotForce = target.aim.right * target.power * target.tankParts.tankWeapons.weapon.speed * target.tankParts.tankWeapons.weapon.speed;
        Debug.Log($"Shot Force  X = {shotForce.x} : Y = {shotForce.y} ");
        if (draw)
        {
            DrawTrajectory(shotForce);
        }
        
        if (shotForce.magnitude < drawBound && draw)
        {
            for (int i = 0; i < dots.Length; i++)
                dots[i].gameObject.SetActive(false);

            draw = false;
        }

        if (shotForce.magnitude > drawBound && !draw)
        {
            for (int i = 0; i < dots.Length; i++)
                dots[i].gameObject.SetActive(true);

            draw = true;
        }
    }

    public void DrawTrajectory(Vector2 shotForce)
    {
        for (int k = 0; k < numberOfDots; k++)
        {
            x1 = ballPos.position.x + shotForce.x * Time.fixedDeltaTime * (dotSeparation * k + dotShift);
            y1 = 
                ballPos.position.y + shotForce.y * Time.fixedDeltaTime * (dotSeparation * k + dotShift)
                - (-Physics2D.gravity.y / 2f * Time.fixedDeltaTime * Time.fixedDeltaTime 
                * (dotSeparation * k + dotShift) * (dotSeparation * k + dotShift)
            );

            dots[k].transform.position = new Vector3(x1, y1, dots[k].transform.position.z);
        }
    }
}
