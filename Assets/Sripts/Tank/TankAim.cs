﻿using UnityEngine;

public class TankAim : MonoBehaviour
{
    public Transform aim;
    public Transform tankBody;

    [HideInInspector]
    public float power, angle;
    [HideInInspector]
    public float powerOffset, angleOffset;
    private bool setAiming = false;

    private Vector2 lastDir;
    private Vector3 aimStartPos;
    private FloatingJoystick aimJoystick;
    [HideInInspector]
    public TankParts tankParts;

    private void Awake()
    {
        tankParts = GetComponent<TankParts>();
        aim = tankParts.gun;
        tankBody = tankParts.body;

        aimJoystick = FindObjectOfType<FloatingJoystick>();

        aimStartPos = aim.localPosition; 
    }

    private void Start()
    {
        WeaponManager.instance.tankAim = this;
    }

    public void Update()
    {
        if (GameManager.instance.gameState != GameManager.State.Player)
            return;

        if (aimJoystick == null)
            aimJoystick = FindObjectOfType<FloatingJoystick>();

        if (setAiming)
        {
            power = Mathf.Abs((aimJoystick.Direction).magnitude);// * tankParts.tankWeapons.weapon.speed;

            aim.right = (aimJoystick.Direction + lastDir).normalized;

            angle = Mathf.Abs(aim.eulerAngles.z);
        }

        if (aim.right.x < 0)
        {
            tankBody.localScale = new Vector3(-1, 1, 1);
            aim.localPosition = new Vector3(-aimStartPos.x, aimStartPos.y, aimStartPos.z);
        }
        else if (aim.right.x > 0)
        {
            tankBody.localScale = new Vector3(1, 1, 1);
            aim.localPosition = aimStartPos;
        }
    }

    public void ClearOffset()
    {
        setAiming = true;
    }

    public void SetOffset()
    {
        powerOffset = power;
        angleOffset = angle;

        lastDir = aim.right;

        setAiming = false;
    }

    public void ResetGun(WeaponData newWeapon)
    {
        tankParts.tankWeapons.weapon = newWeapon;

        power = Mathf.Abs(aimJoystick.Direction.magnitude);

        aim.right = (aimJoystick.Direction + lastDir).normalized;

        angle = Mathf.Abs(aim.eulerAngles.z);

        if (aim.right.x < 0)
        {
            tankBody.localScale = new Vector3(-1, 1, 1);
            aim.localPosition = new Vector3(-aimStartPos.x, aimStartPos.y, aimStartPos.z);
        }
        else if (aim.right.x > 0)
        {
            tankBody.localScale = new Vector3(1, 1, 1);
            aim.localPosition = aimStartPos;
        }
    }
}
