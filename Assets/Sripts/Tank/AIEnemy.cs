﻿using UnityEngine;

public class AIEnemy : MonoBehaviour
{
    [System.Serializable]
    public enum State
    {
        Idle,
        Move,
        Shoot
    }
    public State state;

    private Transform tankBody;
    private Transform aim;
    public bool IsDead = false;
    public int Id; 
    private float wantedDistance = 5;
    private float destinationX;
    private float distance;
    private TankController tankController;
    private TankWeapons tankWeapons;
    private Health health;
    private Transform player;
    private Vector3 aimStartPos;
    private Vector2 destination;
    private TankParts parts;

    private void OnEnable()
    {
        if (health == null)
            health = GetComponent<Health>();

        if (tankController == null)
            tankController = GetComponent<TankController>();

        health.OnDeath += Death;
        tankController.OnOutOfGas += OutOfGas;
    }

    private void OnDisable()
    {
        health.OnDeath -= Death;
        tankController.OnOutOfGas -= OutOfGas;
    }

    public void Init()
    {
        parts = GetComponent<TankParts>();
        tankBody = parts.body;
        aim = parts.gun;
        aimStartPos = aim.localPosition;

        if (tankController == null)
            tankController = GetComponent<TankController>();
        tankWeapons = GetComponent<TankWeapons>();

        if (health == null)
            health = GetComponent<Health>();

        player = FindObjectOfType<Player>().transform;

        if (player.position.x < transform.position.x)
        {
            tankBody.localScale = new Vector3(-1, 1, 1);

            aim.right = -transform.right;

            aim.localPosition = new Vector3(-aimStartPos.x, aimStartPos.y, aimStartPos.z);
        }
        else
        {
            tankBody.localScale = new Vector3(1, 1, 1);

            aim.right = transform.right;

            aim.localPosition = aimStartPos;
        }
    }

    private void Update()
    {
        if (player == null || GameManager.instance.gameState != GameManager.State.Enemy)
            return;

        distance = Vector2.Distance(transform.position, player.position);

        if (state == State.Move)
        {
            Move();
        }

        if (state == State.Shoot)
        {
            if (player.position.x < transform.position.x)
            {
                tankBody.localScale = new Vector3(-1, 1, 1);
                aim.localPosition = new Vector3(-aimStartPos.x, aimStartPos.y, aimStartPos.z);
            }
            else
            {
                tankBody.localScale = new Vector3(1, 1, 1);
                aim.localPosition = aimStartPos;
            }

            Vector2 dir = (((Vector2)transform.position - (Vector2)player.position).normalized - (Vector2.up * 0.5f)).normalized;

            aim.right = Vector2.Lerp(aim.right, -dir, 0.1f);

            if (CloseEnoughPosition(aim.right, -dir, 0.1f))
            {
                state = State.Idle;
                Invoke ("Attack", 1);
            }
        }   
    }

    private bool CloseEnoughPosition(Vector2 v1, Vector2 v2, float acceptableDifference)
    {
        if (Mathf.Abs(v1.x - v2.x) > acceptableDifference)
            return false;

        if (Mathf.Abs(v1.y - v2.y) > acceptableDifference)
            return false;

        return true;
    }

    private bool CloseEnoughFloat(float a, float b, float acceptableDifference)
    {
        return Mathf.Abs(a - b) <= acceptableDifference;
    }

    private void Attack()
    {
        tankWeapons.weapon = WeaponManager.instance.GetWeapon();
        tankWeapons.Shoot(aim.right * (distance * Random.Range(1.6f, 2.00f))); 

        GameManager.instance.Wait();

        CancelInvoke("Attack");
    }

    private void MoveRight()
    {
        tankController.SpeedInput(1);
            tankBody.localScale = new Vector3(1, 1, 1);
    }

    private void MoveLeft()
    {
        tankController.SpeedInput(-1);
            tankBody.localScale = new Vector3(-1, 1, 1);
    }

    private void Move()
    {
        if (!CloseEnoughFloat(transform.position.x, destinationX, 1f) && tankController.gas > 0.1f && tankController.canMove)
        {
            if (transform.position.x < destinationX)
            {
                MoveRight();
            }
            else
            {
                MoveLeft();
            }
        }
        else
        {
            tankController.SpeedInput(0);
            state = State.Shoot;
        }
    }

    private void Death()
    {
        IsDead = true;
        GameManager.instance.enemys.RemoveAll(x=>x.IsDead);
        GameManager.instance.PLayerWon(); 
        Destroy(gameObject);
    }

    private void OutOfGas()
    {
        state = State.Shoot;
    }

    public void Activate(int id)
    { 
        if (player == null)
            return;
        health.EfectCounter();
        if (this.Id != id)
        {
            state = State.Idle;
            return;
        }

        wantedDistance = Random.Range(6, 10);

        if (transform.position.x < player.position.x)
        {
            destinationX = player.position.x + wantedDistance;

            if (destinationX > GameManager.instance.rightBound)
                destinationX = player.position.x - wantedDistance;
        }
        else
        {
            destinationX = player.position.x - wantedDistance;

            if (destinationX < GameManager.instance.leftBound)
                destinationX = player.position.x + wantedDistance;
        }
         
        state =  State.Move;

        tankController.gas = tankController.maxGas;

        tankController.canMove = health.RocetEfect != RocetEfect.Frost;
    }
}
