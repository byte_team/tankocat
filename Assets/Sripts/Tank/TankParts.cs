﻿using UnityEngine;

public class TankParts : MonoBehaviour
{
    public TankInfo info;
    public Transform firePoint;
    public ParticleSystem shootEffect;
    public Transform gun;
    public Transform body;
    public TankWeapons tankWeapons;
    public SpriteRenderer catPlace;
}
