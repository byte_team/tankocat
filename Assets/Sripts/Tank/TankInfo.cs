﻿using UnityEngine;

[CreateAssetMenu(fileName = "Tank Info", menuName = "New Tank Info")]
public class TankInfo : ScriptableObject
{
    public string tankName;
    public Sprite icon;
    public int cost;
    public int hp = 100;
    public int fuel = 10;
    public GameObject prefab;

    [HideInInspector]
    public int available;
    [HideInInspector]
    public int bought;
}
