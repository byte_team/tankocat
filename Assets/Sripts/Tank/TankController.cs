﻿using UnityEngine;

public class TankController : MonoBehaviour
{
    public delegate void OutOfGasAction();
    public event OutOfGasAction OnOutOfGas;

    [System.Serializable]
    public class Stats
    {
        public float maxSpeed = 2f;
        public float length = 0.2f;
        public float height = 0.1f;
    }
    public Stats myStats;

    public LayerMask grounLayer;
    [HideInInspector]
    public float maxGas = 10;
    [HideInInspector]
    public float gas = 10;
    public bool canMove = true;
    public Animator anim;
    public AudioSource engineSound;

    private float h;
    private float speed;
    private float startHeight;
    private Vector2 velocity;
    public Vector2 lastPos;
    private Rigidbody2D rb;

    private void Awake()
    {
        TankInfo info = GetComponent<TankParts>().info;

        lastPos = transform.position;

        startHeight = myStats.height;

        if (info != null)
            maxGas = info.fuel;

        rb = GetComponent<Rigidbody2D>();

        speed = myStats.maxSpeed;

        if (engineSound == null)
            engineSound = GetComponent<AudioSource>();
    }

    private void Update()
    {
        ApplyExplosionForce();
        PositionOnTerrain2();

        if (!canMove)
        {
            engineSound.volume = 0;
            return;
        }

        engineSound.volume = Mathf.Abs(h);

        gas -= Vector2.Distance(transform.position, lastPos);

        lastPos = transform.position;

        if (gas <= 0)
        {
            gas = 0;
            canMove = false;

            if (OnOutOfGas != null)
                OnOutOfGas();
        }
    }

    private void FixedUpdate()
    {
        if (!canMove)
        {
            rb.velocity = Vector2.zero;
            return;
        }

        velocity = Vector2.Lerp(velocity, transform.right * h * speed, 0.05f);

        if (rb.position.x <= GameManager.instance.leftBound && velocity.x < 0)
        {
            velocity.x = 0.1f;
        }
        else if (rb.position.x >= GameManager.instance.rightBound && velocity.x > 0)
        {
            velocity.x = -0.1f;
        }

        rb.velocity = velocity;    
    }

    private void PositionOnTerrain2()
    {
        RaycastHit2D left = Physics2D.Raycast(transform.position + transform.up - (transform.right * myStats.length), -transform.up, 50f, grounLayer);
        RaycastHit2D right = Physics2D.Raycast(transform.position + transform.up + (transform.right * myStats.length), -transform.up, 50f, grounLayer);

        if (left.transform.tag != "Ground")
            return;

        Quaternion targetRotation = Quaternion.FromToRotation(Vector3.up, (left.normal + right.normal) / 2);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 0.1f);
        transform.position = ((left.point + right.point) / 2) + (Vector2)transform.up * myStats.height;
    }

    public void SpeedInput(float value)
    {
        h = value;
        anim.SetFloat("Speed", h);
    }

    public void SetExplosionForce(Vector2 force)
    {
        startForce = force;
        isJumping = true;
        jumpTime = 0;
    }

    private Vector2 startForce;

    private Vector2 currentExpForce, expForce;

    private bool isJumping = false;
    private float jumpTime = 0;

    private void ApplyExplosionForce()
    {
        if (isJumping)
        {
            jumpTime += Time.deltaTime * 6;

            if (jumpTime < 1)
                currentExpForce = Vector2.Lerp(currentExpForce, startForce, jumpTime);
            else if (jumpTime >= 1)
                currentExpForce = Vector2.Lerp(startForce, Vector2.zero, jumpTime - 1);

            if (jumpTime >= 2)
            {
                isJumping = false;
                jumpTime = 0;
                anim.SetTrigger("Fall");
            }
        }

        myStats.height = currentExpForce.y + startHeight;
        transform.position += transform.right * currentExpForce.x * Time.deltaTime * 10;
    }

    public void ResetFuel()
    {
        if (transform != null)
        lastPos = transform.position;
        gas = maxGas;
    }
}
