﻿using UnityEngine;

public class TankWeapons : MonoBehaviour
{
    public WeaponData weapon;
    public Animator anim;

    private Transform shootPoint;
    private TankAim aim;
    private ParticleSystem shootEffect;

    private void Awake()
    {
        aim = GetComponent<TankAim>();

        TankParts parts = GetComponent<TankParts>();
        shootPoint = parts.firePoint;
        shootEffect = parts.shootEffect;
    }

    public void Shoot(Vector2 velocity)
    {
        Rigidbody2D missle = Instantiate(weapon.rocket, shootPoint.position, Quaternion.identity).GetComponent<Rigidbody2D>();
        int starCount = PlayerPrefs.GetInt(weapon.name + " stars", 0);
        missle.GetComponent<Rocket>().damage += starCount * weapon.damageMultuplier;
        missle.GetComponent<Rocket>().efect = weapon.Efect;
        missle.velocity = velocity;

        shootEffect.Play();

        anim.SetTrigger("Shoot");
    }
}
