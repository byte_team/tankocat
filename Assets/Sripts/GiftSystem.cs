﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GiftSystem : MonoBehaviour {
    public GameObject emptyGift;
    public GameObject BackButton;


    public void CLickGift()
    {
        emptyGift.SetActive(true);
        this.gameObject.SetActive(false);
        BackButton.SetActive(true);
    }
}
