﻿using Cinemachine;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;

    public Animator cameraAnimator;
    public CinemachineVirtualCamera spesificCamera;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    public void ZoomTo(Transform target)
    {
        spesificCamera.Follow = target;

        cameraAnimator.SetBool("Specific", true);
    }

    public void ZoomOut(float wait)
    {
        Invoke("InternalZoomOut", wait);
    }

    private void InternalZoomOut()
    {
        cameraAnimator.SetBool("Specific", false);
    }
}
