﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class Gift : MonoBehaviour {

    public static Gift instance;

    public int coinAmount;
    public ulong timeToWaitMilSeconds;
    [Space]
    public GameObject unavaiable;
    public Animator attentionAnim;
    public Button giftButton;
    public Text timerGift;
    public Text coinText;

    private ulong lastGiftOpen;
    private bool getNow = false;
    bool result;

    private void Awake()
    {
        instance = this;
        coinText.text = coinAmount.ToString();
    }

    void Start ()
    {
        Advertisements.Instance.Initialize();
       

        result = ulong.TryParse(PlayerPrefs.GetString("lastGiftOpen"),out lastGiftOpen);

        if(result)
          lastGiftOpen = ulong.Parse(PlayerPrefs.GetString("lastGiftOpen"));
        if (!Interactable())
            giftButton.interactable = false;
	}

	void Update ()
    {
        if (timerGift.text == "-596523h -35791394m 48s " || timerGift.text == "596523h 35791394m 32s " || getNow)
        {
            giftButton.interactable = true;
            timerGift.text = "";
            unavaiable.SetActive(false);
        }

        if (!giftButton.IsInteractable())
        {
            if (Interactable())
            {
                giftButton.interactable = true;
                timerGift.text = "";
                return;
            }

            unavaiable.SetActive(true);

            //Set the timer
            ulong diff = (ulong)DateTime.Now.Ticks - lastGiftOpen;
            ulong m = diff / TimeSpan.TicksPerMillisecond;
            float timeLeftSec = (float)(timeToWaitMilSeconds - m) / 1000.0f;

            string r = "";

            //Hours
            r += ((int)timeLeftSec / 3600).ToString() + "h ";
            timeLeftSec -= ((int)timeLeftSec / 3600) * 3600;

            //Minutes
            r += ((int)timeLeftSec / 60).ToString("00") + "m ";
            //Seconds
            r += (timeLeftSec % 60).ToString("00") + "s ";
            timerGift.text = r.ToString();

            if (attentionAnim.isActiveAndEnabled)
                attentionAnim.enabled = false;
        }
        else if (!attentionAnim.isActiveAndEnabled)
            attentionAnim.enabled = true;
    }

    public void Click()
    {
        PlayerPrefs.SetInt("amountOfMoney",PlayerPrefs.GetInt("amountOfMoney")+40);
        lastGiftOpen = (ulong)DateTime.Now.Ticks;
        PlayerPrefs.SetString("lastGiftOpen", lastGiftOpen.ToString());
        PlayerPrefs.Save();
        giftButton.interactable = false;

        PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins", 0) + coinAmount);
        UIMenu.instance.UpdateCoins();

        SoundManager.instance.BuySound();

        getNow = false;
    }

    private bool Interactable()
    {
        ulong diff = (ulong)DateTime.Now.Ticks - lastGiftOpen;
        ulong m = diff / TimeSpan.TicksPerMillisecond;
        
        ulong timeLeftSec = (ulong)(timeToWaitMilSeconds - m) / 1000;

        if (timeLeftSec < 0)
        {
            giftButton.interactable = true;
            return true;
        }
        else
            return false;
    }
    public void WatchAd()
    {
        Advertisements.Instance.ShowRewardedVideo(CompleteMethod);
    }
    private void CompleteMethod(bool completed, string advertiser)
    {
        Debug.Log("Closed rewarded from: " + advertiser + " -> Completed " + completed);
        if (completed == true)
        {
            GetGiftNow();
        }
        else
        {
            //no reward 
        }
    }
    public void GetGiftNow()
    {
        getNow = true;
    }
}
