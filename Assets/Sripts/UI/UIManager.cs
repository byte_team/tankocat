﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public GameObject playerUI;
    public GameObject lostPanel;
    public Image fuelBar;
    public Image playerWeaponIcon;
    public Image enemyIcon;

    [Header("Damage")]
    public Transform HUDCanvas;
    public GameObject damagePopup;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    public void DamagePopup(int damage, Vector3 pos)
    {
        GameObject newCoinPopup = Instantiate(damagePopup, HUDCanvas);
        Vector3 newPos = Camera.main.WorldToScreenPoint(pos);
        newPos.x = Mathf.Clamp(newPos.x, 150, Screen.width - 150);
        newPos.y = Mathf.Clamp(newPos.y, 150, Screen.height - 150);

        newCoinPopup.GetComponentInChildren<Text>().text = damage.ToString();
        newCoinPopup.transform.position = newPos;
    }

    public void UpdateFuel(float value, float max)
    {
        fuelBar.fillAmount = value / max;
    }
}
