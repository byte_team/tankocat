﻿using UnityEngine.UI;
using UnityEngine;

public class UIEditor : MonoBehaviour
{
    [System.Serializable]
    public class UIGroup
    {
        public string name;
        public Color color;
        public Image[] images;
    }
    public UIGroup[] UIGroups;

    private void OnValidate()
    {
        foreach(UIGroup group in UIGroups)
        {
            foreach(Image image in group.images)
            {
                image.color = group.color;
            }
        }
    }
}
