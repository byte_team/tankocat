﻿using UnityEngine;

public class UIWeapons : MonoBehaviour
{
    public GameObject weaponInfoPrefab;

    public void CreateInfoPanel(WeaponData weapon)
    {
        UIWeaponHolder newHolder = Instantiate(weaponInfoPrefab, transform).GetComponent<UIWeaponHolder>();
        newHolder.weapon = weapon;
        newHolder.UpdateInfo();
    }
}
