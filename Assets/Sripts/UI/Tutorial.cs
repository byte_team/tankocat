﻿using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public int showUpTimes = 3;

    private void Awake()
    {
        int shown = PlayerPrefs.GetInt("Tutorial", 1);

        if (shown > showUpTimes)
            gameObject.SetActive(false);
        else
        {
            PlayerPrefs.SetInt("Tutorial", shown + 1);
        }
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
