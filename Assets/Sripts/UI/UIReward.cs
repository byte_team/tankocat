﻿using UnityEngine.UI;
using UnityEngine;

public class UIReward : MonoBehaviour
{
    public static UIReward instance;

    public Text coinsEarnedText, totalCoinsText;
    public Animator tankListAnim;

    private int coinsEarned, coins;

    private void Awake()
    {
        instance = this;

        coins = PlayerPrefs.GetInt("Coins", 0);
        coinsEarned = PlayerPrefs.GetInt("Coins Earned", 0);

        UpdateCoins();

        ShowTank(PlayerPrefs.GetString("Tank In Use", "T-34"));
    }

    public void UpdateCoins()
    {
        coinsEarnedText.text = "+ " + coinsEarned.ToString();
        totalCoinsText.text = coins.ToString();
    }

    public void GetReward()
    {
        coins += coinsEarned;

        PlayerPrefs.SetInt("Coins", coins);

        UpdateCoins();

        coinsEarned = 0;
        PlayerPrefs.SetInt("Coins Earned", 0);
    }

    public void GetDoubleReward(bool complete)
    {
        if (complete)
        {  
            if (GameManager.instance.reward)
            {
                coins += coinsEarned * 2;

                PlayerPrefs.SetInt("Coins", coins);

                UpdateCoins();

                coinsEarned = 0;
                PlayerPrefs.SetInt("Coins Earned", 0);

                GameManager.instance.reward = false;
            }
            if (GameManager.instance.gift)
            {
                Gift.instance.GetGiftNow();
                GameManager.instance.gift = false;
            }
        }
    }

    public void WatchAds()
    {
        Advertisements.Instance.ShowRewardedVideo(GetDoubleReward);
    }

    private void ShowTank(string tankName)
    {
        switch (tankName)
        {
            case "T-34":
                tankListAnim.SetInteger("Tank Index", 0);
                break;
            case "Desert Cat":
                tankListAnim.SetInteger("Tank Index", 1);
                break;
            case "Steel Crumbler":
                tankListAnim.SetInteger("Tank Index", 2);
                break;
            case "Assasin":
                tankListAnim.SetInteger("Tank Index", 3);
                break;
            case "Scavanger":
                tankListAnim.SetInteger("Tank Index", 4);
                break;
            default:
                tankListAnim.SetInteger("Tank Index", 0);
                break;

        }
    }
}
