﻿using UnityEngine;

public class CameraZoom : MonoBehaviour
{
    public float zoomSpeed = 5;
    public float minZoom = 1.5f, maxZoom = 5.9f;

    public bool canZoom = false;

    public Cinemachine.CinemachineVirtualCamera targetCamera;

    private Vector3 touchStart;

    private void FixedUpdate()
    {
        if (GameManager.instance.gameState == GameManager.State.Waiting || !canZoom)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            float difference = currentMagnitude - prevMagnitude;

            Zoom(difference * zoomSpeed);
        }
        else if (Input.GetMouseButton(0))
        {
            Vector3 direction = touchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetCamera.transform.position += direction;
        }

        Zoom(Input.GetAxis("Mouse ScrollWheel") * zoomSpeed);

        print(Input.GetAxis("Mouse ScrollWheel") * zoomSpeed);
    }

    private void Zoom(float increment)
    {
        targetCamera.m_Lens.OrthographicSize = Mathf.Clamp(Camera.main.orthographicSize - increment, minZoom, maxZoom);

        print(increment);
    }

    public void ActivateZoom()
    {
        canZoom = true;
    }

    public void DeactivateZoom()
    {
        canZoom = false;
    }
}
