﻿using UnityEngine;

public class MenuTank : MonoBehaviour
{
    public GameObject rocket;
    public Transform shootPoint;
    public AudioClip shootSound;

    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void OnMouseDown()
    {
        anim.SetTrigger("Shoot");

        Instantiate(rocket, shootPoint.position, rocket.transform.rotation);
        SoundManager.instance.audioSource.PlayOneShot(shootSound);
    }
}
