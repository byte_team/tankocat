﻿using UnityEngine.UI;
using UnityEngine;

public class UILevelHolder : MonoBehaviour
{
    public LevelInfo[] levelInfoList;

    public Image icon;
    public Image progressBar;
    public Text progressText;
    public GameObject lockObject;
    public GameObject playButton;

    private int currentIndex = 0;

    private void OnEnable()
    {
        if (LevelLoader.instance != null)
        {
            LevelCheck();
        }
        else
            Invoke("LevelCheck", 0.1f);
    }

    private void LevelCheck()
    {
        string lastLevel = PlayerPrefs.GetString("Last Level", "Desert");

        for (int i = 0; i < levelInfoList.Length; i++)
        {
            if (lastLevel == levelInfoList[i].name)
            {
                ShowInfo(levelInfoList[i]);
                currentIndex = i;
                return;
            }
        }
    }

    private void ShowInfo(LevelInfo info)
    {
        icon.sprite = info.levelIcon;
        int levelsDone = PlayerPrefs.GetInt(info.levelName + " progress", 0);
        progressBar.fillAmount = (float)levelsDone / info.levelsToUnlockNext;
        progressText.text = levelsDone + "/" + info.levelsToUnlockNext;

        if (info.isUnlocked())
        {
            PlayerPrefs.SetString("Last Level", info.name);

            LevelLoader.instance.selectedLevelID = info.sceneID;

            lockObject.SetActive(false);
            if (playButton != null)
                playButton.SetActive(true);
        }
        else
        {
            lockObject.SetActive(true);
            if (playButton != null)
                playButton.SetActive(false);
        }
    }

    public void GoLeft()
    {
        if (currentIndex == 0)
        {
            //currentIndex = levelInfoList.Length - 1;
            return;
        }
        else
            currentIndex--;

        ShowInfo(levelInfoList[currentIndex]);
    }

    public void GoRight()
    {
        if (currentIndex == levelInfoList.Length - 1)
        {
            //currentIndex = 0;
            return;
        }
        else
            currentIndex++;

        ShowInfo(levelInfoList[currentIndex]);
    }
}
