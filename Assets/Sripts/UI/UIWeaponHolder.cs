﻿using UnityEngine.UI;
using UnityEngine;

public class UIWeaponHolder : MonoBehaviour
{
    public Image icon;
    public Text weaponName;
    public Text damageText;
    public WeaponData weapon;
    public bool autoUpdate = false;

    public GameObject[] stars;

    private void Awake()
    {
        if (weapon != null)
            UpdateInfo();
    }

    public void UpdateInfo()
    {
        icon.sprite = weapon.icon;
        weaponName.text = weapon.rocketName;

        int starCount = PlayerPrefs.GetInt(weapon.name + " stars", 0);

        int totalDamage = weapon.rocket.GetComponent<Rocket>().damage + starCount * weapon.damageMultuplier;
        damageText.text = totalDamage.ToString();

        for (int i = 0; i < starCount; i++)
        {
            stars[i].SetActive(true);
        }
    }

    public void SetWeapon()
    {
        WeaponManager.instance.SetPlayerWeapon(weapon);
        WeaponManager.instance.weaponPanelAnim.SetTrigger("Open");

        SoundManager.instance.ConfirmSound();
    }

    public void SetWeaponToUpgrade()
    {
        UpgradeManager.instance.SelectWeapon(this);
    }
}
