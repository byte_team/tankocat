﻿using UnityEngine;

[CreateAssetMenu(fileName ="New Level Info", menuName = "Create Level Info")]
public class LevelInfo : ScriptableObject
{
    public string levelName;
    public LevelInfo levelThatUnlcoksMe;
    public Sprite levelIcon;
    public int levelsToUnlockNext = 10;
    public int sceneID = 1;

    public bool isUnlocked()
    {
        if (levelThatUnlcoksMe == null)
            return true;

        int levelsDone = PlayerPrefs.GetInt(levelThatUnlcoksMe.levelName + " progress", 0);

        if (levelsDone >= levelThatUnlcoksMe.levelsToUnlockNext)
            return true;
        else
            return false;
    }
}
