﻿using UnityEngine.UI;
using UnityEngine;

public class UITankHolder : MonoBehaviour
{
    public TankInfo info;
    public Image icon;
    public Animator anim;
    public Text cost;
    public GameObject locked;
    public bool isPreUnlocked = false;

    private void Awake()
    {
        icon.sprite = info.icon;
        cost.text = info.cost.ToString();

        if (PlayerPrefs.GetString("Tank In Use", "T-34") == info.tankName)
        {
            UIMenu.instance.tankSelector.transform.position = transform.position;
        }

        if (isPreUnlocked)
        {
            PlayerPrefs.SetInt(info.name + " unlocked", 1);
        }

        if (PlayerPrefs.GetInt(info.name + " unlocked", 0) == 1)
            locked.SetActive(false);
    }

    public void SetPlayerTank()
    {
        if (PlayerPrefs.GetInt(info.name + " unlocked", 0) == 0)
        {
            if (PlayerPrefs.GetInt(info.name + " unlocked", 0) == 0 && PlayerPrefs.GetInt("Coins", 0) >= info.cost)
                BuyTank();
            else
            {
                SoundManager.instance.DeniedSound();

                return;
            } 
        }

        UIMenu.instance.ChangeTank(info);
        UIMenu.instance.tankSelector.transform.position = transform.position;
        UIMenu.instance.tankInUseName.text = info.tankName;
        PlayerPrefs.SetString("Tank In Use", info.tankName);

        SoundManager.instance.ConfirmSound();
    }

    public void BuyTank()
    {
        PlayerPrefs.SetInt(info.name + " unlocked", 1);
        locked.SetActive(false);

        SoundManager.instance.BuySound();

        PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins", 0) - info.cost);

        UIMenu.instance.UpdateCoins();
    }
}
