﻿using UnityEngine.UI;
using UnityEngine;

public class UIBuyCoins : MonoBehaviour
{
    public float price = 2f;
    public Text priceText;
    [Space]
    public int coinAmount = 1000;
    public Text coinAmountText;

    private void Awake()
    {
        priceText.text = price + "$";
        coinAmountText.text = coinAmount.ToString();
    }

    public void Buy()
    {
        // check money

        PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins", 0) + coinAmount);
        SoundManager.instance.BuySound();

        UIMenu.instance.UpdateCoins();
    }
}
