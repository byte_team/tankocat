﻿using UnityEngine.UI;
using UnityEngine;

public class UIHealthbar : MonoBehaviour
{
    public float speed = 0.2f;
    public float delay = 0.8f;
    public float delaySpeed = 0.02f;

    public Image fillImage, delayImage;
    [HideInInspector]
    public Health target;

    private float mainFill;
    private float delayFill;

    private void OnEnable()
    {
        target.OnDamaged += ChangeFill;
    }

    private void OnDisable()
    {
        target.OnDamaged -= ChangeFill;
    }

    private void Awake()
    {
        fillImage.fillAmount = 1;
        mainFill = 1;

        delayImage.fillAmount = 1;
        delayFill = 1;
    }

    private void Update()
    {
        if (fillImage.fillAmount != mainFill)
        {
            fillImage.fillAmount = Mathf.Lerp(fillImage.fillAmount, mainFill, speed * Time.deltaTime);
        }

        if (delayImage.fillAmount != delayFill)
        {
            delayImage.fillAmount = Mathf.Lerp(delayImage.fillAmount, delayFill, delaySpeed * Time.deltaTime);
        }
    }

    public void ChangeFill()
    {      
        mainFill = (float)target.health / target.maxHealth;

        Invoke("ChangeDelayFill", delay);
    }

    private void ChangeDelayFill()
    {
        delayFill = mainFill;
    }
}
