﻿using UnityEngine.UI;
using UnityEngine;

public class UISettings : MonoBehaviour
{
    public Image targetImage;
    public Color activeColor, inactiveColor;

    public string settingName;
    public bool isActive = true;

    private void Start()
    {
        if (PlayerPrefs.GetInt(settingName, 1) == 1)
            isActive = true;
        else
            isActive = false;

        UpdateState();
    }

    private void UpdateState()
    {
        if (isActive)
        {
            targetImage.color = activeColor;

            SoundManager.instance.mainMixer.SetFloat(settingName, 0);
        }
        else
        {
            targetImage.color = inactiveColor;

            SoundManager.instance.mainMixer.SetFloat(settingName, -80);
        }
    }

    public void SetSettings()
    {
        isActive = !isActive;

        if (isActive)
            PlayerPrefs.SetInt(settingName, 1);
        else
            PlayerPrefs.SetInt(settingName, 0);

        UpdateState();
    }
}
