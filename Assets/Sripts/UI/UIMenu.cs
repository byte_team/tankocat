﻿using UnityEngine.UI;
using UnityEngine;
using System;

public class UIMenu : MonoBehaviour
{
    public static UIMenu instance;

    public GameObject menuOptionsPanel, tankChoosePanel, shopPanel;
    [Space]
    public Text tankInUseName;
    public Text coinsText;
    public Animator tankInfoAnim;
    public Animator tankListAnim;
    public Animator logoAnim;

    public Image tankSelector;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        if (tankInUseName != null)
            tankInUseName.text = PlayerPrefs.GetString("Tank In Use", "T-34");

        ShowTank(PlayerPrefs.GetString("Tank In Use", "T-34"));

        UpdateCoins();
    }

    public void ChangeTank(TankInfo newTank)
    {
        tankInUseName.text = newTank.tankName;
        tankInfoAnim.SetTrigger("Change");

        ShowTank(newTank.tankName);
    }

    public void UpdateCoins()
    {
        coinsText.text = PlayerPrefs.GetInt("Coins", 0).ToString();
    }

    private void ShowTank(string tankName)
    {
        switch (tankName)
        {
            case "T-34":
                tankListAnim.SetInteger("Tank Index", 0);
                break;
            case "Desert Cat":
                tankListAnim.SetInteger("Tank Index", 1);
                break;
            case "Steel Crumbler":
                tankListAnim.SetInteger("Tank Index", 2);
                break;
            case "Assasin":
                tankListAnim.SetInteger("Tank Index", 3);
                break;
            case "Scavanger":
                tankListAnim.SetInteger("Tank Index", 4);
                break;
            default:
                tankListAnim.SetInteger("Tank Index", 0);
                break;

        }
    }

    public void MenuOptions()
    {
        menuOptionsPanel.SetActive(true);
        tankChoosePanel.SetActive(false);
        shopPanel.SetActive(false);

        logoAnim.SetBool("Show", true);
        tankInfoAnim.SetBool("Show", false);       
    }

    public void TankChoose()
    {
        menuOptionsPanel.SetActive(false);
        tankChoosePanel.SetActive(true);
        shopPanel.SetActive(false);

        logoAnim.SetBool("Show", false);
        tankInfoAnim.SetBool("Show", true);
    }

    public void Shop()
    {
        menuOptionsPanel.SetActive(false);
        tankChoosePanel.SetActive(false);
        shopPanel.SetActive(true);

        logoAnim.SetBool("Show", false);
        tankInfoAnim.SetBool("Show", true);
    }

    public void Exit()
    {
        Environment.Exit(0);
    }
}
