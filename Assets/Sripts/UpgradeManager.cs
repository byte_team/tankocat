﻿using UnityEngine.UI;
using UnityEngine;

public class UpgradeManager : MonoBehaviour
{
    public static UpgradeManager instance;

    public UIWeaponHolder startHolder;

    private WeaponData selectedWeapon;
    private UIWeaponHolder currentHolder;

    [Header("UI")]
    public Image icon;
    public Text weaponName;
    public Text upgradeCost;

    private void Awake()
    {
        instance = this;

        SelectWeapon(startHolder);
    }

    public void SelectWeapon(UIWeaponHolder holder)
    {
        WeaponData newWeapon = holder.weapon;
        currentHolder = holder;

        selectedWeapon = newWeapon;

        icon.sprite = newWeapon.icon;
        weaponName.text = newWeapon.rocketName;
        int starCount = PlayerPrefs.GetInt(selectedWeapon.name + " stars", 0);
        upgradeCost.text = (newWeapon.baseCost * (starCount + 1)).ToString();
    }

    public void Upgrade()
    {
        int starCount = PlayerPrefs.GetInt(selectedWeapon.name + " stars", 0);
        int coins = PlayerPrefs.GetInt("Coins", 0);
        int currentCost = selectedWeapon.baseCost * (starCount + 1);

        if (currentCost <= coins)
        {
            if (starCount < 5)
            {
                PlayerPrefs.SetInt(selectedWeapon.name + " stars", starCount + 1);

                PlayerPrefs.SetInt("Coins", coins - currentCost);
                UIMenu.instance.UpdateCoins();

                SoundManager.instance.BuySound();

                SelectWeapon(currentHolder);
            }
            else
                SoundManager.instance.DeniedSound();
        }
        else
        {
            SoundManager.instance.DeniedSound();
        }

        currentHolder.UpdateInfo();
    }
}
