﻿using UnityEngine;

public class RansdomRotation : MonoBehaviour
{
    public float range = 45f;

    private void Awake()
    {
        float angle = Random.Range(-range, range);

        transform.localEulerAngles = Vector3.forward * angle;
    }
}
