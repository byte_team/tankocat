﻿using UnityEngine;
public enum RocetEfect
{
    None, Fire, Frost
}
public class Health : MonoBehaviour
{
   

    public delegate void DeathAction();
    public event DeathAction OnDeath;
    public delegate void DamageAction();
    public event DamageAction OnDamaged;

    public int maxHealth = 100;
    public GameObject deathEffect;
    public Animator anim;

    [HideInInspector]
    public int health; 
    public RocetEfect RocetEfect = RocetEfect.None;

    private void Awake()
    {
        TankInfo info = GetComponent<TankParts>().info; 
        if (info != null)
            maxHealth = info.hp;

        health = maxHealth;
    }

    private int efectCounter;
    public void EfectCounter()
    { 
        if (efectCounter == 0)
        {
            RocetEfect = RocetEfect.None;
        }
        else
        {
            efectCounter--;
        }
    }

    public void Damage(int amount,RocetEfect efect, Vector3 popupOffset)
    { 
        if(RocetEfect != efect && efect!=RocetEfect.None)
        {
            RocetEfect = efect;
            efectCounter = 3;
        }
        if(efect== RocetEfect.Fire)
        { 
            health -= amount * 2;
        }
        health -= amount ; 

        UIManager.instance.DamagePopup(amount, transform.position + popupOffset);
        anim.SetTrigger("Damage");

        OnDamaged?.Invoke();

        if (health <= 0)
        {
            health = 0;

            Instantiate(deathEffect, transform.position, Quaternion.identity);

            if (OnDeath != null)
                OnDeath();
        }
    }
}
