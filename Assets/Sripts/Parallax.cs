﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public Transform targetCamera;
    [Range(0, 1)]
    public float parallaxH = 1, parallaxV = 1;

    private float width, height, startPosH, startPosV;
    private float tempH, distanceH, tempV, distanceV;

    private Vector3 parallaxPos;

    private void Start()
    {
        if (targetCamera == null)
            targetCamera = Camera.main.transform;

        startPosH = transform.position.x;
        startPosV = transform.position.y;

        width = GetComponent<SpriteRenderer>().bounds.size.x;
        height = GetComponent<SpriteRenderer>().bounds.size.y;

        parallaxPos = transform.position;
    }

    private void FixedUpdate()
    {
        tempH = targetCamera.position.x * (1 - parallaxH);
        distanceH = targetCamera.position.x * parallaxH;

        //if (tempH > startPosH + width)
        //    startPosH += width;
        //else if (tempH < startPosH - width)
        //    startPosH -= width;

        tempV = targetCamera.position.y * (1 - parallaxV);
        distanceV = targetCamera.position.y * parallaxV;

        //if (tempV > startPosV + height)
        //    startPosV += height;
        //else if (tempV < startPosV - height)
        //    startPosV -= height;


        transform.position = new Vector3(startPosH + distanceH, startPosV + distanceV, transform.position.z);
    }
}
