﻿using UnityEngine;

[CreateAssetMenu(fileName ="Weapon", menuName = "Create Weapon")]
public class WeaponData : ScriptableObject
{
    public string rocketName;
    public float speed = 16;
    public GameObject rocket;
    public Sprite icon;
    public RocetEfect Efect;
    public int baseCost = 100;
    public int costMultiplier = 2;
    public int damageMultuplier = 2;
}
