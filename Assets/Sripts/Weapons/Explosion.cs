﻿using UnityEngine;

public class Explosion : MonoBehaviour
{
    public int damage = 20;
    public float radius = 3;
    public CapsuleCollider2D holeCol;
    public GameObject explosionParticles;
    public GameObject misc;
    public AudioClip sound;

    private bool spawnedMisc = false;

    private void Awake()
    {
        GameManager.instance.NextState(3);
        CameraController.instance.ZoomOut(2);
        ShakerManager.instance.Shake();

        SoundManager.instance.audioSource.PlayOneShot(sound);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Health enemyHealth = collision.GetComponent<Health>();

        if (enemyHealth != null)
        {
            float distance = Vector2.Distance(transform.position, collision.transform.position);
            if (distance > radius)
                distance = radius;

            int realDamage = (int)(damage * ((radius - distance) / radius));

            enemyHealth.Damage(realDamage,RocetEfect.None, Vector2.right * 0.3f);
        }     

        TankController tank = collision.GetComponent<TankController>();

        if (tank != null)
        {
            Vector2 expForce = new Vector2(0.01f, 0.02f) * damage;
            if (transform.position.x >  collision.transform.position.x)
                expForce.x = -expForce.x;

            tank.SetExplosionForce(expForce);
        }

        Destruction2D destruction2D = collision.GetComponent<Destruction2D>();
        if (destruction2D != null && destruction2D.isActiveAndEnabled)
        {
            destruction2D.DestroyByCollider(holeCol);
        }

        if (!spawnedMisc)
        {
            Instantiate(misc, transform.position, Quaternion.identity);
            Instantiate(explosionParticles, transform.position, Quaternion.identity);
            spawnedMisc = true;
        }

        Destroy(gameObject);
    }
}
