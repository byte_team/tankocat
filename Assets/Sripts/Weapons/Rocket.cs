﻿using UnityEngine;

public class Rocket : MonoBehaviour
{
    [SerializeField]
    public enum RocketType
    {
        Simple,
        SelfAiming,
        Spreading,
        ActionInitiator
    }
    public RocketType rocketType;

    public GameObject explosion;
    public int damage = 10;
    public RocetEfect efect;
    public AudioClip sound;

    private bool spawnExplosion = false;
    private Rigidbody2D rb;
    private Transform enemy;

    private void Awake()
    {
        CameraController.instance.ZoomTo(transform);
        rb = GetComponent<Rigidbody2D>();

        SoundManager.instance.audioSource.PlayOneShot(sound);
    }

    private void Update()
    {
        transform.up = rb.velocity.normalized;

        if (rocketType == RocketType.SelfAiming)
            SelfAiming();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!spawnExplosion)
        {
            Instantiate(explosion, transform.position, Quaternion.identity);
            spawnExplosion = true;
        }

        if (collision.gameObject.GetComponent<Health>() != null)
            collision.gameObject.GetComponent<Health>().Damage(damage, efect, Vector2.left * 0.3f);

        Destroy(gameObject);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Boundary")
        {
            if (!spawnExplosion)
            {
                Instantiate(explosion, transform.position, Quaternion.identity);
                spawnExplosion = true;
            }
            Destroy(gameObject);
        }
    }

    private void SelfAiming()
    {
        RaycastHit2D hitDown = Physics2D.Raycast(transform.position, Vector2.down, 200f, 1 << 9);

        if (hitDown != false)
        {
            if (hitDown.collider.tag == "Enemy")
            {
                enemy = hitDown.transform;

                print(hitDown.collider.name);
            }
        }

        if (enemy != null)
        {
            Vector2 dir = enemy.position - transform.position;
            dir.Normalize();

            transform.right = dir;

            print(dir);
        }
    }
}
