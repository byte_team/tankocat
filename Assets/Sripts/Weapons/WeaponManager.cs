﻿using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public static WeaponManager instance;

    public UIWeapons weaponPanel;
    public WeaponData[] weapons;
    [HideInInspector]
    public WeaponData playerWeapon;
    public Animator weaponPanelAnim;

    [HideInInspector]
    public TankAim tankAim;
    [HideInInspector]
    public TankWeapons player;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        foreach (WeaponData weapon in weapons)
        {
            weaponPanel.CreateInfoPanel(weapon);
        }
    }

    private void Start()
    {
        //SetPlayerWeapon(weapons[0]);
    }

    public WeaponData GetWeapon()
    {
        int r = Random.Range(0, weapons.Length);

        return weapons[r];
    }

    public void SetPlayerWeapon(WeaponData weapon)
    {
        if (weapon == null)
            weapon = weapons[0];

        playerWeapon = weapon;

        UIManager.instance.playerWeaponIcon.sprite = weapon.icon;

        if (player == null)
            player = FindObjectOfType<Player>().tankWeapons;

        player.weapon = weapon;
    }
}
